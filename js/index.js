/*
Задание No1. Дочерние элементы в DOM
Для страницы:
<html>
<body>
<div>Пользователи:</div>
<ul>
<li>Джон</li>
<li>Пит</li>
</ul>
</body>
</html>
Как получить:
• Напишите код, который получит элемент <div>?
• Напишите код, который получит <ul>?
• Напишите код, который получит второй <li> (с именем Пит)?
 */
let getDiv = document.querySelector('div');
let getUl = document.querySelector('ul');
let getLi = document.querySelector('li:last-child');
/*
Задание No2. Выделите ячейки по диагонали
Напишите код, который выделит красным цветом все ячейки в таблице по диагонали.
Вам нужно получить из таблицы <table> все диагональные <td> и выделить их,

используя код:
// в переменной td находится DOM-элемент для тега <td>
td.style.backgroundColor = 'red';
Результат:
 */
let getTable = document.querySelector('table');
for (let i = 0; i < getTable.rows.length; i++) {
    let getRow = getTable.rows[i];
    getRow.cells[i].style.backgroundColor = 'red';
}
/*
Задание No3. Поиск элементов
Вот документ с таблицей и формой. Как найти?...
• Таблицу с id="age-table".
• Все элементы label внутри этой таблицы (их три).
• Первый td в этой таблице (со словом «Age»).
• Форму form с именем name="search".
• Первый input в этой форме.
• Последний input в этой форме.
Используйте код файла table.html и браузерные инструменты разработчика:
<html>
<body>
<form name="search">
<label>Search the site:
<input type="text" name="search">
</label>
<input type="submit" value="Search!">
</form>
<hr>
<form name="search-person">
Search the visitors:
<table id="age-table">
<tr>
<td>Age:</td>
<td id="age-list">
<label>
<input type="radio" name="age" value="young">less than
18</label>
<label>
<input type="radio" name="age" value="mature">18-50</label>
<label>
<input type="radio" name="age" value="senior">more than
50</label>
</td>
</tr>
<tr>
<td>Additionally:</td>
<td>
<input type="text" name="info[0]">
<input type="text" name="info[1]">
<input type="text" name="info[2]">
</td>
</tr>
</table>

<input type="submit" value="Search!">
</form>
</body>
</html>
 */
let table = document.getElementById('age-table');
console.log(table.getElementsByTagName('label'));
table.querySelector('td');
let formSearch = document.querySelector('form[name="search"]');
formSearch.querySelector('input');
let inputs = formSearch.querySelectorAll('input');

/*
Задание No4. Очистите элемент
Создайте функцию clear(elem), которая удаляет всё содержимое из elem.
<ol id="elem">
<li>Привет</li>
<li>Мир</li>
</ol>
<script>
function clear(elem) {
}
clear(elem); // очищает список
</script>
 */
function clear(elem) {
    while (elem.firstChild) {
        elem.firstChild.remove();
    }
}

/*
Задание No5. Создайте список
Напишите интерфейс для создания списка.
Для каждого пункта:
1. Запрашивайте содержимое пункта у пользователя с помощью prompt.
2. Создавайте элемент <li> и добавляйте его к <ul>.
3. Процесс прерывается, когда пользователь нажимает Esc или вводит пустую
строку.
Все элементы должны создаваться динамически.
Если пользователь вводит HTML-теги -– пусть в списке они показываются как обычный
текст.
 */

let ul = document.createElement('ul');
document.body.append(ul);

while (true) {
    let text = prompt("Enter text", "");

    if (!text) {
        break;
    }

    let createLi = document.createElement('li');
    createLi.textContent = text;
    ul.append(createLi);
}
/*
Задание No6. Вставьте HTML в список
Напишите код для вставки <li>2</li><li>3</li> между этими двумя <li>:
<ul id="ul">
<li id="one">1</li>
<li id="two">4</li>
</ul>
//one.insertAdjacentHTML('afterend', '<li>2</li><li>3</li>');
 */

/*
Задание No7. Создать уведомление
Напишите функцию showNotification(options), которая создаёт уведомление: <div
class="notification"> с заданным содержимым. Уведомление должно автоматически
исчезнуть через 1,5 секунды.

Пример объекта options:
// показывает элемент с текстом "Hello" рядом с правой верхней частью окна.
showNotification({
top: 10, // 10px от верхней границы окна (по умолчанию 0px)
right: 10, // 10px от правого края окна (по умолчанию 0px)
html: "Hello!", // HTML-уведомление
className: "welcome" // дополнительный класс для div (необязательно)
});
 */

